import java.util.ArrayList;
import java.util.Random;

class ContoCorrente {
    private String intestatario;
    private int saldo;
    private ArrayList<DatiOperazione> movimenti;

    public ContoCorrente(String intestatario, int saldo) {
        this.intestatario = intestatario;
        this.saldo = saldo;
        movimenti = new ArrayList<>();
    }

    // deposita
    public void deposita(int somma, String nomeUt) {
        delay();
        this.saldo += somma;
        movimenti.add(new DatiOperazione(somma, nomeUt));
    }

    // preleva
    public void preleva(int somma, String nomeUt) {
        delay();
        this.saldo -= somma;
        movimenti.add(new DatiOperazione(-somma, nomeUt));
    }

    // stampa movimenti
    public void stampaListaMovimenti() {
        movimenti.forEach((e) -> {
            System.out.println(e);
        });
    }

    public void delay() {
        try {
            Thread.sleep((long) Math.random() * 100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String getIntestatario() {
        return this.intestatario;
    }

    public String toString() {
        return "Intestatario: " + this.intestatario + "\nSaldo: " + this.saldo + "\nNumero Operazioni: "
                + movimenti.size();
    }
}

// classe Operatore
class Operatore extends Thread {
    private String name;
    private ContoCorrente account;
    private Random rnd = new Random(); // random

    public Operatore(String name, ContoCorrente account) {
        this.name = name;
        this.account = account;
    }

    public void run() {
        boolean randomBooleand = rnd.nextBoolean();

        if (randomBooleand) {
            account.deposita(10, this.name);
        } else {
            account.preleva(10, this.name);
        }
    }
}

class DatiOperazione {
    private int importo;
    private String operatore;

    public DatiOperazione(int importo, String operatore) {
        this.importo = importo;
        this.operatore = operatore;
    }

    public String toString() {
        return "Operatore: " + operatore + "\nImporto: " + importo;
    }
}

public class Main {
    public static void main(String[] args) {
        ContoCorrente contoCorrente = new ContoCorrente("intestatario", 0);

        Operatore[] operatores = new Operatore[10];

        for (Operatore op : operatores) {
            op = new Operatore("Andrea", contoCorrente);
            op.start();
            try {
                op.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println(contoCorrente.toString());
    }
}