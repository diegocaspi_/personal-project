import java.util.Random;
import java.util.Scanner;

import jdk.javadoc.internal.doclets.toolkit.taglets.SystemPropertyTaglet;

public class ArraySort {
    static Scanner sc = new Scanner(System.in);
    static Random rnd = new Random(); // random object used for the length and for the elements of the array
    static int[] array; // full array
    static int[] th1; // the first part of the array given for sorting to Thread t1
    static int[] th2; // the secondo part of the array given for sorting to Thread t2

    public static void main(String[] args) {
        System.out.println("Inserire la lunghezza dell'array: ");
        array = new int[sc.nextInt()]; // initilize the array with the user length
        
        for (int i = 0; i < array.length; i++)
            array[i] = rnd.nextInt(5000); // fill the array with random elements

        System.out.println("Array disordinato:");
        printArray(array); // print the messy array

        if (array.length % 2 == 0) { // divide the array in two different arrays
            th1 = new int[array.length / 2];
            th2 = new int[th1.length];
        } else {
            th1 = new int[(array.length - 1) / 2];
            th2 = new int[(array.length + 1) / 2];
        }

        for (int i = 0; i < th1.length; i++) // fill the two array with the elements of the main array
            th1[i] = array[i];
        for (int i = th1.length; i < array.length; i++)
            th2[i - th1.length] = array[i];

        MyThread t1 = new MyThread(th1); // initialize and run the first Thread
        MyThread t2 = new MyThread(th2); // initialize and run the second Thread

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Array ordinato:");
        array = merge(th1, th2); //merge the two sorted arrays
        printArray(array); //print the main sorted array

    }

    //merge algorithm
    static int[] merge(int[] a1, int[] a2){
        int[] ret = new int[a1.length + a2.length];

        int index1 = 0;
        int index2 = 0;
        int i = 0;

        while(index1 < a1.length && index2 < a2.length){
            if(a1[index1] < a2[index2])
                ret[i++] = a1[index1++];
            else
                ret[i++] = a2[index2++];
        }
        while(index1 < a1.length)
            ret[i++] = a1[index1++];
        while(index2< a2.length)
            ret[i++] = a2[index2++];

        return ret;
    }

    static void printArray(int[] ar){
        System.out.print("[ ");
        for(int i : ar)
            System.out.print(i + " ");
        System.out.println("]");
    }
}

class MyThread extends Thread{
    private int[] a;

    MyThread(int[] a){
        this.a = a;
        run();
    }

    public void run(){
        sort(a);
    }

    //bubble sort algorithm
    static void sort(int[] a){
        for(int y = 0; y < a.length; y++){
            for(int i = 0; i < a.length - 1; i++){
                if(a[i] > a[i + 1]){
                    int temp = a[i];
                    a[i] = a[i + 1];
                    a[i + 1] = temp;
                }
            } 
        }
    }
}