import java.util.ArrayList;
import java.util.Scanner;

/*
    @author Diego Caspi
    Find all dividers of number from 0 to a number chosen from user
*/
public class Dividers{
    static Scanner sc = new Scanner(System.in); //initialize Scanner object
    public static void main(String[] args) {
        
        System.out.println("Inserire il limite: ");
        int val = sc.nextInt(); //store the range of numbers

        ArrayList<Integer>[] matrix = new ArrayList[val]; //create the matrix Object with VAL elements

        for(int i = 0; i < val; i++)
            matrix[i] = new ArrayList<Integer>();

        long start = System.currentTimeMillis(); //store the start time to calculate the time used to complete the operations
        for(int i = 0; i < val + 1; i++)
            new ThreadDivisori(i, matrix).start(); //run all Thread to use multithreading

        long end = System.currentTimeMillis(); //store the end time
        printMatrix(matrix);

        System.out.println();
        System.out.println("Tempo impiegato per il calcolo: " + (end - start) + " millisecondi");
        System.out.println("Tempo medio per operazione: " + (double)(end - start) / val + " millisecondi");
    }

    //method used to print the matrix
    static void printMatrix(ArrayList<Integer>[] matrix){
        int cont = 1;
        for(ArrayList<Integer> a : matrix){
            System.out.print("[num " + cont + "] ");
            if(a.isEmpty())
                System.out.print("Primo");
            else {
                for(Integer i : a)
                    System.out.print(i + " ");
            }
            System.out.println();
            cont++;
        }
    }
}

class ThreadDivisori extends Thread{
    private int num;
    private ArrayList<Integer>[] matrix;

    ThreadDivisori(int num, ArrayList<Integer>[] matrix){
        this.num = num;
        this.matrix = matrix;
    }

    public void run(){
        for(int i = 2; i < num; i++){
            if(num % i == 0)
                matrix[num - 1].add(i);
        }
    }
}