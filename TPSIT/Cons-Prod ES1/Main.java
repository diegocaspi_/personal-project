
//class Produttore
class Produttore extends Thread {
    ContoCorrente cc;

    public Produttore(ContoCorrente cc) {
        this.cc = cc;
    }

    public void run() {
        synchronized(cc) {
            for (int i = 0; i < 20; i++)
                cc.deposito(10);
        }
    }
}

// class Consumatore
class Consumatore extends Thread {
    ContoCorrente cc;

    public Consumatore(ContoCorrente cc) {
        this.cc = cc;
    }

    public void run() {
        synchronized(cc){
            for (int i = 0; i < 20; i++)
                cc.prelievo(10);
        }
    }
}

// class ContoCorrente
class ContoCorrente {
    private String intestatario;
    private int saldo;

    public ContoCorrente(String intestatario, int saldo) {
        this.intestatario = intestatario;
        this.saldo = saldo;
    }

    public void deposito(int somma) {
        delay();
        this.saldo -= somma;
        // System.out.println(toString() + "\nDopo il deposito");
    }

    public void prelievo(int somma) {
        delay();
        this.saldo += somma;
        // System.out.println(toString() + "\nDopo il prelievo");
    }

    public void delay() {
        try {
            Thread.sleep((long) (Math.random() * 100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String toString(){
        return "Intestatario: " + this.intestatario + "\nSaldo: " + this.saldo;
    }
}

public class Main{
    public static void main(String[] args) throws InterruptedException {
        ContoCorrente c = new ContoCorrente("Diego", 500);
        Produttore producer = new Produttore(c);
        Consumatore consumer = new Consumatore(c);

        //starting producer thread
        producer.start();
        //starting consumer thread
        consumer.start();
        producer.join();
        consumer.join();

        System.out.println(c);
    }
}