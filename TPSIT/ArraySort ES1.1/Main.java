import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main{

    static Scanner sc = new Scanner(System.in); //scanner object
    static Random rnd = new Random(); //random object
    static Integer[] mainArray; //main array
    static List<List<Integer>> partitions = new LinkedList<List<Integer>>(); //list of list to store every part of the array
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Inserire la lunghezza dell'array: ");
        mainArray = new Integer[sc.nextInt()];

        //verify if the number of splitted arrays is bigger than main array length
        int numParts = Integer.MAX_VALUE;
        while(numParts > mainArray.length){
            System.out.println("Inserire il numero di parti in cui suddividere l'array: ");
            numParts = sc.nextInt();

            if(numParts > mainArray.length)
                System.out.println("Numero troppo grande, riprova!");
        }

        List<Integer> mainList = Arrays.asList(mainArray); //convert array in a list and fill it with random numbers
        for (int i = 0; i < mainArray.length; i++)
            mainArray[i] = rnd.nextInt(5000);
        
        for(Integer i : mainList) //print list
            System.out.print(i + " ");

        System.out.println();

        //find the max partition size and the number of partition with max size
        int maxSize = (int) Math.ceil((double) mainList.size() / numParts);
        int minSize = maxSize - 1;

        int partMaxNum = 0;
        int partMinNum = 0;

        if((mainList.size() % numParts) == 0){
            partMaxNum = numParts;
            partMinNum = 0;
        } else {
            if((mainList.size() / 2) > numParts){
                partMaxNum = (int) Math.floor((double) mainList.size() / numParts);
                partMinNum = numParts - partMaxNum;
            } else {
                partMaxNum = mainList.size() - numParts;
                partMinNum = numParts - partMaxNum;
            }
        }
        
        for(int i = 0; i < partMaxNum * maxSize; i += maxSize)
            partitions.add(mainList.subList(i, Math.min(i + maxSize, mainList.size())));

        for(int i = partMaxNum * maxSize; i < (partMaxNum * maxSize) + (partMinNum * minSize); i += minSize)
            partitions.add(mainList.subList(i, Math.min(i + minSize, mainList.size())));

        for(List<Integer> list : partitions){
            new SortThread(list).join();
        }

        int cont = 1;
        for(List<Integer> list : partitions){
            System.out.print("Part " + cont + ": ");
            for(Integer i : list)
                System.out.print(i + " ");
            System.out.println();
            cont++;
        }

        int[] f = bigMerge(new int[0], partitions, 0);

        System.out.print("Array ordinato: ");
        for(int i : f)
            System.out.print(i + " ");
    }

    static int[] bigMerge(int[] end, List<List<Integer>> partitions, int index){
        if(index < partitions.size()){
            int[] ret = merge(end, toIntArray(partitions.get(index)));
            return bigMerge(ret, partitions, index + 1);
        } else {
            return end;
        }
    }

    static int[] merge(int[] a1, int[] a2){
        int[] ret = new int[a1.length + a2.length];

        int index1 = 0;
        int index2 = 0;
        int i = 0;

        while(index1 < a1.length && index2 < a2.length){
            if(a1[index1] < a2[index2])
                ret[i++] = a1[index1++];
            else
                ret[i++] = a2[index2++];
        }
        while(index1 < a1.length)
            ret[i++] = a1[index1++];
        while(index2< a2.length)
            ret[i++] = a2[index2++];

        return ret;
    }

    static int[] toIntArray(List<Integer> list){
        int[] ret = new int[list.size()];
        for(int i = 0; i < ret.length; i++)
            ret[i] = list.get(i);
        return ret;
    }
}

class SortThread extends Thread{
    private List<Integer> array;

    public SortThread(List<Integer> array) {
        this.array = array;
        run();
    }
    public void run(){
        sort(array);
    }

    //bubble sort algorithm
    void sort(List<Integer> a){
        for(int y = 0; y < a.size(); y++){
            for(int i = 0; i < a.size() - 1; i++){
                if(a.get(i) > a.get(i + 1)){
                    int temp = a.get(i);
                    a.set(i, a.get(i + 1));
                    a.set(i + 1, temp);
                }
            } 
        }
    }
}