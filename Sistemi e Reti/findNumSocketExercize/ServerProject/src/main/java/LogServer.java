import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class LogServer{
    public static final int PORT = 8080; //porta di comunicazione

    public static void main(String[] args) {
        Socket socket = null;
        ServerSocket serverSocket = null;
        //tentativo di connessione alla porta
        try{
            serverSocket = new ServerSocket(PORT);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //cerca host per sempre e se si verifica una connessione avvia un ServeThread nel socket corrente
        while(true){
            try{
                socket = serverSocket.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }
            new ServerThread(socket).start();
        }
    }
}