import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

class ServerThread extends Thread{
    private Socket socket; //istanza del socket
    private ArrayList<Integer> secretNumber = null; //numero magico
    private String secretString = ""; //numero magico in "String"

    /*
    Costruttore che accetta come parametro il socket
    dell'utente corrente
    */
    ServerThread(Socket socket){
        this.socket = socket;
    }

    public void run(){
        //istanza degli utils per leggere e scrivere sul buffer del socket corrente
        DataInputStream in;
        DataOutputStream out = null;
        try {
            in =  new DataInputStream(new BufferedInputStream(socket.getInputStream()));
            out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            //se si verifica un eccezione di tipo IO termina il thread
            return;
        }
        secretNumber = generateNum(); //genero il numero casuale
        secretString = listToString(secretNumber); //converto il numero magico in stringa
        System.out.println("Secret number = " + secretString);

        String clientMsg = ""; //messaggio del client

        //finchè il client non invia il numero corretto il thread continua
        while(!clientMsg.equals(secretString)){
            try {
                clientMsg = in.readUTF();
            } catch (IOException e) {
                //questa eccezione si verifica se il client lascia improvvisamente la connessione con il socket
                return;
            }

            //debug
            if (clientMsg.equals("QUIT")){
                System.out.println("QUITTING CURRENT SOCKET");
                try {
                    socket.close();
                    in.close();
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            String result = calcolateResult(clientMsg);
            System.out.println(result);
            try {
                out.writeUTF(result);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            socket.close();
            in.close();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
    Metodo utilizzato per eseguire il controllo e calcolare il risultato
    della stringa passata come parametro, tale metodo ritorna una stringa
    che rappresenta il messaggio che il Server deve inviare al Client
     */
    private String calcolateResult(String input){
        char[] elements = input.toCharArray();
        int posCounter = 0;
        int elementsCounter = 0;

        //calcolo gli elementi nella giusta posizione
        for(int i = 0; i < secretNumber.size(); i++) {
            if (Character.getNumericValue(elements[i]) == secretNumber.get(i))
                posCounter += 1;
        }
        //calcolo gli elementi contenuti corretti
        for(char item : elements){
            if (secretNumber.contains(Character.getNumericValue(item)))
                elementsCounter += 1;
        }

        return "POS:" + posCounter + " ELE:" + (elementsCounter - posCounter);
    }

    /*
    Metodo utilizzato per convertire il numero magico
    in "String"
     */
    private String listToString(ArrayList<Integer> list){
        String ret = "";
        for(Integer i : list)
            ret += Integer.toString(i);
        return ret;
    }

    /*
    Metodo utilizzato per creare il numero segreto nel momento
    in cui viene startato il Thread dell'utente corrente
    */
    private ArrayList<Integer> generateNum(){
        ArrayList<Integer> ret = new ArrayList<Integer>();
        Random rnd = new Random();

        while(ret.size() < 4){
            int val = rnd.nextInt(10);
            if(!ret.contains(val))
                ret.add(val);
        }
        return ret;
    }
}