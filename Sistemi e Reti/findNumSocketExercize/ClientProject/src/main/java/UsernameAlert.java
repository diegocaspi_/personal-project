import javafx.scene.control.Alert;

/*
Classe utilizzata per creare un Alert se il nome utente non è corretto
 */
public class UsernameAlert extends Alert {
    public UsernameAlert(AlertType alertType) {
        super(alertType);
    }

    public void showAlert(){
        this.setTitle("WARNING DIALOG");
        this.setHeaderText("Username non valido");
        this.setContentText("Il nome utente inserito non è valido!");
        this.showAndWait();
    }
}
