import javafx.scene.control.Alert;

/*
Classe utilizzata per creare un Alert se il nome utente non è corretto
 */
public class GameInputAlert extends Alert {
    public GameInputAlert(AlertType alertType) {
        super(alertType);
    }

    public void showAlert(){
        this.setTitle("WARNING DIALOG");
        this.setHeaderText("Errore nel contenuto del testo che hai inserito");
        this.setContentText("Controlla il testo che hai inserito, tipi di errore: \n-Lunghezza errata\n-Caratteri non consentiti\n-Caratteri ripetuti");
        this.showAndWait();
    }
}
