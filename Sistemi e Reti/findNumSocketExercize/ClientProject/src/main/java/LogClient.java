import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

public class LogClient extends Application {
    private static final int PORT  = 8080; // indirizzo della porta di accesso del socket
    private int movesCounter = 0; // contatore di mosse
    private int correctPosCounter = 0; // contatore di elementi nella posizione corretta
    private int correctValuesCounter = 0; // contatore di elementi corretti nella posizione sbagliata

    private StackPane stackPane = new StackPane(); // pannello di base in cui vengono aggiunti e rimossi i pannelli delle varie scene

    private String user = ""; //nome dell'utente corrente

    public static void main(String[] args) {
        launch(args);
    }

    //creo la scena iniziale, gli associo il file css, aggiungo allo stackPane l'interfaccia iniziale e mostro lo stage
    public void start(Stage stage) throws Exception {
        Scene scene = new Scene(stackPane, 600, 400, Color.CADETBLUE);
        scene.getStylesheets().add(getClass().getClassLoader().getResource("style.css").toExternalForm());
        stackPane.getChildren().add(initUI());
        stage.setScene(scene);
        stage.setTitle("Indovina il numero");
        stage.show();
    }

    /*
    Funzione utilizzata per controllare se tutte le cifre
    sono convertibili in intero
     */
    private boolean isIntegerConvertible(String s){
        char[] elements = s.toCharArray();

        for(char c : elements){
            if(!Character.isDigit(c))
                return false;
        }
        return true;
    }

    /*
    Funzione utilizzata per controllare se una stringa ha tutti
    caratteri diversi al suo interno
     */
    private boolean hasDifferentElements(String s){
        char[] elements = s.toCharArray();
        ArrayList<Integer> diffs = new ArrayList<>();

        for (char item : elements){
            if(diffs.contains(Character.getNumericValue(item)))
                return false;
            else
                diffs.add(Character.getNumericValue(item));
        }
        return true;
    }

    /*
    TYPE > Animation
    Funzione utilizzata per spostare la scena corrente nella
    scena di gioco
     */
    private void doFadeIn(Pane paneToRemove, Pane paneToAdd, StackPane root){
        root.getChildren().add(paneToAdd);
        FadeTransition fadeTransition = new FadeTransition(Duration.millis(600));
        fadeTransition.setOnFinished(evt -> {
            root.getChildren().remove(paneToRemove);
        });
        fadeTransition.setNode(paneToAdd);
        fadeTransition.setFromValue(0);
        fadeTransition.setToValue(1);
        fadeTransition.play();
    }

    /*
    Funzione per settare l'interfaccia corrente
    in quella iniziale
     */
    private GridPane initUI(){
        //reset values
        movesCounter = 0;
        correctPosCounter = 0;
        correctValuesCounter = 0;
        user = "";

        Scene beginScene = null;
        GridPane root = new GridPane();
        root.setId("root");

        //modifico le proprietà del GridPane "root"
        root.setAlignment(Pos.CENTER);
        root.setHgap(10);
        root.setVgap(10);
        root.setPadding(new Insets(25, 25, 25, 25));

        //istanza di tutti gli elementi necessari
        Button starter = new Button("INIZIA");
        Label title = new Label("Benvenuto!");
        Label username = new Label("Username: ");
        TextField userField = new TextField();
        HBox box = new HBox(10);

        //modifico le proprietà del Label "title"
        title.setFont(Font.font("Tahoma", FontWeight.BLACK, 20));
        //modifico le proprietà dell'HBox box
        box.setAlignment(Pos.BOTTOM_RIGHT);
        box.getChildren().add(starter);

        //modifico le proprietà del Button "starter"
        starter.setId("startButton");
        starter.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if(userField.getText().equals("")){
                    UsernameAlert alert = new UsernameAlert(Alert.AlertType.WARNING);
                    alert.showAlert();
                } else {
                    user = userField.getText();
                    Socket socket = null;
                    try {
                        socket = new Socket("localhost", PORT);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    doFadeIn(root, inGameUI(socket), stackPane);
                }
            }
        });

        //aggiungo tutti gli elementi nel GridPane root
        root.add(title, 0, 0, 2, 1);
        root.add(username, 0, 1);
        root.add(userField, 1, 1);
        root.add(box, 1, 4);

        return root;
    }

    /*
    Funzione utilizzata per creare la scena in gioco
     */
    private BorderPane inGameUI(Socket socket){

        BorderPane root = new BorderPane();
        root.setId("root");

        //istanza di tutti gli elementi
        Label topLabel = new Label("INDOVINA IL NUMERO\nUsername: " + user + "\nMosse: " + movesCounter);
        Label correct = new Label("Cifre nella giusta posizione (PG): " + correctPosCounter + "\nCifre in posizione non corretta (PE): " + correctValuesCounter);
        TextField field = new TextField();
        Button send = new Button("INVIA");

        field.setId("inGameField");
        //list view
        final ObservableList<String> data = FXCollections.observableArrayList();
        ListView<String> previousNumbers = new ListView<>();
        //lista di strighe data selezionata come lista per la ListView
        previousNumbers.setItems(data);

        correct.setStyle("-fx-font-weight: bold");

        //modifica le proprietà della Label "topLabel"
        topLabel.setStyle("-fx-font-weight: bold");
        DropShadow dropShadow = new DropShadow();
        dropShadow.setOffsetX(0);
        dropShadow.setOffsetY(0);
        topLabel.setEffect(dropShadow);

        //modifico le proprietà del Buttoon "send"
        send.setPadding(new Insets(10, 10, 10 ,10));
        send.setId("sendButton");
        send.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                DataInputStream in = null;
                DataOutputStream out = null;

                try {
                    in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
                    out = new DataOutputStream(socket.getOutputStream());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String fieldText = field.getText();
                //controllo che il testo inserito dall'utente sia corretto
                if (fieldText.length() != 4 || !isIntegerConvertible(fieldText) || !hasDifferentElements(fieldText)){
                    GameInputAlert alert = new GameInputAlert(Alert.AlertType.WARNING);
                    alert.showAlert();
                } else {
                    //incremento il numero di mosse
                    movesCounter += 1;
                    topLabel.setText("INDOVINA IL NUMERO\nUsername: " + user + "\nMosse: " + movesCounter);
                    try {
                        out.writeUTF(field.getText());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //leggo la risposta dal server
                    String serverReply = "";
                    try {
                        serverReply = in.readUTF();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //decode server message
                    String[] values = serverReply.split(" ");
                    for(String value : values){
                        if(value.charAt(0) == 'P')
                            correctPosCounter = Character.getNumericValue(value.charAt(4));
                        else if(value.charAt(0) == 'E')
                            correctValuesCounter = Character.getNumericValue(value.charAt(4));
                    }
                    data.add("'" + fieldText + "' PG: " + correctPosCounter + "   PE: " + correctValuesCounter);

                     //se il numero di valori giusti e il numero di valori nella posizione giusta
                    //sono uguali a 0 e a 4 vuol dire che il gioco è finito
                    if (correctValuesCounter == 0 && correctPosCounter == 4){
                        try {
                            out.writeUTF("QUIT");
                            socket.close();
                            in.close();
                            out.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        doFadeIn((BorderPane) stackPane.getChildren().get(0), endGameUI(), stackPane);
                    }
                    //resetto il contenuto del TextField
                    field.setText("");

                    //aggiorno il contenuto della Label "correct"
                    correct.setText("Cifre nella giusta posizione (PG): " + correctPosCounter + "\nCifre in posizione non corretta (PE): " + correctValuesCounter);
                }
            }
        });

        //modifico le proprietà del la ListView prevoiusNumber
        previousNumbers.setPrefWidth(200);
        previousNumbers.setMaxWidth(200);

        //modifico le proprietà del TextField "field"
        field.setPrefWidth(80);
        field.setMaxWidth(80);

        //aggiungo gli elementi al BorderPane
        root.setTop(topLabel);
        root.setCenter(field);
        root.setLeft(previousNumbers);
        root.setRight(send);
        root.setBottom(correct);

        //modifico le proprietà del BorderPane "root"
        root.setPadding(new Insets(20, 20, 20, 20));
        root.setAlignment(topLabel, Pos.CENTER);
        root.setAlignment(send, Pos.CENTER_RIGHT);

        return root;
    }

    /*
    Funzione utilizzata per spostare la scena corrente
    nella scena finale
     */
    private BorderPane endGameUI(){

        BorderPane root = new BorderPane();
        root.setId("root");

        //istanza di tutti gli elementi
        Label top = new Label("OTTIMO " + user +"!\n" + "HAI VINTO! ECCO IL RISULTATO FINALE");
        Label center = new Label("");
        Button exitButton = new Button("ESCI");
        exitButton.setId("startButton");

        top.setId("endTopLabel");
        center.setId("endCenterLabel");

        if (movesCounter == 1)
            center.setText("Hai completato con successo il gioco in un tentativo");
        else
            center.setText("Hai completato con successo il gioco in " + movesCounter + " tentativi");

        exitButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                doFadeIn((Pane) stackPane.getChildren().get(0), initUI(), stackPane);
            }
        });

        //inserimento degli elementi del BorderPane root
        root.setTop(top);
        root.setCenter(center);
        root.setRight(exitButton);

        //modifica impostazioni del BorderPane
        root.setPadding(new Insets(20, 20, 20, 20));
        root.setAlignment(top, Pos.CENTER);
        root.setAlignment(exitButton, Pos.BOTTOM_RIGHT);

        return root;
    }
}
