package wrapper;

public class Book {
    private String title;
    private String autore;
    private String prezzo;
    private String editore; //ricavato interrogando la tabella Editori con l'idEdi
    private String luogo; //ricavato interrogando la tabella Luoghi con l'idLuo
    private int idInv;
    private int pubYear;

    public Book(String title, String autore, String prezzo, String editore, String luogo, int idInv, int pubYear){
        this.title = title;
        this.autore = autore;
        this.prezzo = prezzo;
        this.editore = editore;
        this.luogo = luogo;
        this.idInv = idInv;
        this.pubYear = pubYear;
    }

    public String getTitle() {
        return title;
    }

    public String getAutore() {
        return autore;
    }

    public String getPrezzo() {
        return prezzo;
    }

    public String getEditore() {
        return editore;
    }

    public String getLuogo() {
        return luogo;
    }

    public int getIdInv() {
        return idInv;
    }

    public int getPubYear() {
        return pubYear;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", autore='" + autore + '\'' +
                ", prezzo='" + prezzo + '\'' +
                ", editore='" + editore + '\'' +
                ", luogo='" + luogo + '\'' +
                ", idInv=" + idInv +
                ", pubYear=" + pubYear +
                '}';
    }
}