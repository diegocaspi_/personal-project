package bot;

import driver.DatabaseDriver;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.exceptions.TelegramApiValidationException;
import wrapper.Book;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;


public class LibraryBot extends TelegramLongPollingBot {
    private DatabaseDriver driver;

    private TreeMap<Long, Message> organizedMessage;

    public LibraryBot(DatabaseDriver driver){
        this.driver = driver;
        this.organizedMessage = new TreeMap<>();
    }

    public void onUpdateReceived(Update update) {
        if(update.hasMessage() && update.getMessage().hasText()){
            String message_text = update.getMessage().getText();
            long chat_id = update.getMessage().getChatId();
            SendMessage sendMessage;

            if(message_text.equals("/start")) {
                //sending begin message
                sendMessage = getStartMessage(chat_id);
                try {
                    sendMessage.validate();
                    organizedMessage.put(chat_id, execute(sendMessage));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
                //sending selection message
                sendMessage = getSelectionMessage(chat_id);
                try {
                    sendMessage.validate();
                    organizedMessage.put(chat_id, execute(sendMessage));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            }
            else if(message_text.equals("Filtra per titolo del libro")){
                sendMessage = getFilterByTitleMessage(chat_id);
                try{
                    sendMessage.validate();
                    organizedMessage.put(chat_id, execute(sendMessage));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            }
            else if(message_text.equals("Filtra per autore del libro")){
                sendMessage = getFilterByAuthorMessage(chat_id);
                try{
                    sendMessage.validate();
                    organizedMessage.put(chat_id, execute(sendMessage));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            }
            else {
                Message msg = organizedMessage.get(chat_id);
                if(msg.getText().equals("Inserisci il titolo del libro")){
                    try {
                        sendMessage = getBooksFromTitle(message_text, chat_id);
                        sendMessage.validate();
                        organizedMessage.put(chat_id, execute(sendMessage));
                    } catch (SQLException | TelegramApiException throwables) {
                        throwables.printStackTrace();
                    }

                } else if(msg.getText().equals("Inserisci nome e cognome dell'autore del libro")){
                    try{
                        sendMessage = getBooksFromAuthor(message_text, chat_id);
                        sendMessage.validate();
                        organizedMessage.put(chat_id, execute(sendMessage));
                    } catch (TelegramApiException | SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private SendMessage getBooksFromTitle(String title, long chat_id) throws SQLException {
        ArrayList<Book> books = driver.filterByTitle(title);
        System.out.println(books.size());

        InlineKeyboardMarkup replyKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();

        for (Book book : books){
            rows.add(new ArrayList<InlineKeyboardButton>(Arrays.asList(new InlineKeyboardButton(book.getTitle() + "\n " + book.getAutore()).setCallbackData("enter"))){});
        }

        replyKeyboardMarkup.setKeyboard(rows);

        return new SendMessage()
                .setChatId(chat_id)
                .setText("Ecco quello che ho trovato")
                .setReplyMarkup(replyKeyboardMarkup);
    }

    private SendMessage getBooksFromAuthor(String author, long chat_id) throws SQLException{
        ArrayList<Book> books = driver.filterByAuthor(author);

        System.out.println(books.size());

        InlineKeyboardMarkup replyKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();

        for (Book book : books){
            rows.add(new ArrayList<>(Arrays.asList(new InlineKeyboardButton(book.getTitle() + "\n").setCallbackData("enter"))));
        }

        replyKeyboardMarkup.setKeyboard(rows);

        return new SendMessage()
                .setChatId(chat_id)
                .setText("Ecco quello che ho trovato")
                .setReplyMarkup(replyKeyboardMarkup);
    }

    private SendMessage getStartMessage(long chat_id){
        String text = "Benvenuto nel bot telegram che si occupa della biblioteca dell'ITIS C.Zuccante di Mestre \n" +
                "Con questo bot puoi verificare la presenza di un libro cercandolo per nome o per autore";

        return new SendMessage()
                .setChatId(chat_id)
                .setText(text);
    }

    private SendMessage getFilterByTitleMessage(long chat_id) {
        String text = "Inserisci il titolo del libro";

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();

        //inline keyboard markup
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> currentRow = new ArrayList<>();
        currentRow.add(new InlineKeyboardButton(
                "Indietro"
        ).setCallbackData("Indietro"));
        rows.add(currentRow);
        inlineKeyboardMarkup.setKeyboard(rows);

        return new SendMessage()
                .setChatId(chat_id)
                .setReplyMarkup(inlineKeyboardMarkup)
                .setText(text);
    }

    private SendMessage getFilterByAuthorMessage(long chat_id){
        String text = "Inserisci nome e cognome dell'autore del libro";

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();

        //inline keyboard markup
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> currentRow = new ArrayList<>();
        currentRow.add(new InlineKeyboardButton(
                "Indietro"
        ).setCallbackData("Indietro"));
        rows.add(currentRow);
        inlineKeyboardMarkup.setKeyboard(rows);

        return new SendMessage()
                .setChatId(chat_id)
                .setReplyMarkup(inlineKeyboardMarkup)
                .setText(text);
    }

    private SendMessage getSelectionMessage(long chat_id){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

        //reply keyboard markup
        List<KeyboardRow> replyRows = new ArrayList<>();
        //first row
        KeyboardRow row1 = new KeyboardRow();
        row1.add("Filtra per titolo del libro");
        //second row
        KeyboardRow row2 = new KeyboardRow();
        row2.add("Filtra per autore del libro");

        replyRows.add(row1);
        replyRows.add(row2);
        replyKeyboardMarkup.setKeyboard(replyRows);

        // return
        return new SendMessage()
                .setChatId(chat_id)
                .setText("Seleziona il filtro per cercare il libro")
                .setReplyMarkup(replyKeyboardMarkup);
    }

    public String getBotUsername() {
        return "zuccanteLibrary_bot";
    }

    public String getBotToken() {
        return "1232035445:AAG9V0wt4d74JwdYC-b4LQ1K_bVXgfX1byE";
    }
}
