import bot.LibraryBot;
import driver.DatabaseDriver;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;
import wrapper.Book;

import java.sql.*;
import java.util.ArrayList;

public class App {

    public static void main(String[] args) throws SQLException {
        ApiContextInitializer.init();
        DatabaseDriver app = new DatabaseDriver();
        TelegramBotsApi botsApi = new TelegramBotsApi();

        try{
            botsApi.registerBot(new LibraryBot(app));
        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
//        DatabaseDriver driver = new DatabaseDriver();
//        ArrayList<Book> books = driver.filterByAuthor("CALVINO ITALO");
//        System.out.println(books.size());
    }
}