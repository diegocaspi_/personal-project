package driver;

import wrapper.Book;

import java.sql.*;
import java.util.ArrayList;

public class DatabaseDriver {
    private static Connection connection;
    private static Statement statement;

    public DatabaseDriver() throws SQLException {
        connection = connect();
        statement = connection.createStatement();
    }

    //create a connection with database
    private Connection connect(){
        Connection conn = null;
        try{
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + getClass().getResource("../dbBiblioteca.sqlite3").toString());
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }
        return conn;
    }

    public ArrayList<Book> selectAll() throws SQLException {
        ArrayList<Book> ret = new ArrayList<>();
        ResultSet set = connection.createStatement().executeQuery("SELECT * FROM LIBRI");
        while (set.next())
            ret.add(createBook(set));
        return ret;
    }

    //filter research by authour name
    public ArrayList<Book> filterByAuthor(String author) throws SQLException {
        ArrayList<Book> ret = new ArrayList<>(); //return element
        int idAut; //used to find author
        ArrayList<Integer> idInv = new ArrayList<>(); //used to find author

        // cercare l'IdAut nella Tabella "Autori" tramite il nome
        ResultSet rs = statement.executeQuery("SELECT * FROM AUTORI where Aut = '" + author.toUpperCase() + "'");

        //if anything found
        if(rs.isClosed()){
            System.out.println("Nulla trovato");
            return new ArrayList<>();
        }

        idAut = rs.getInt("IdAut");

        // cercare l'IdInv nella Tabella "InvAut" tramite l'IdAut
        rs = statement.executeQuery("SELECT * FROM INVAUT where IdAut = '" + idAut + "'");
        while (rs.next())
            idInv.add(rs.getInt("IdInv"));

        // cercare i libri nella Tabella "Libri" tramite l'IdInv
        idInv.forEach((val) -> {
            try{
                ResultSet set = statement.executeQuery("SELECT * FROM LIBRI where IdInv = '" + val + "'");
                while (set.next()){
                    //creation of a book
                    ret.add(createBook(set));
                }
            } catch (SQLException e){
                e.printStackTrace();
            }
        });
        return ret;
    }

    private Book createBook(ResultSet set) throws SQLException {
        String s = set.getString("Tit");
        String prezzo = set.getString("Pre");
        int idInvElement = set.getInt("idInv");
        int pub = set.getInt("Pub");

        ResultSet editoreSet = connection.createStatement().executeQuery("SELECT * FROM EDITORI where IdEdi = '" +
                set.getInt("IdEdi") + "'");

        ResultSet luogoSet = connection.createStatement().executeQuery("SELECT * FROM LUOGHI where IdLuo = '" +
                set.getInt("IdLuo") + "'");

        int idAuthor = connection.createStatement().executeQuery("SELECT * FROM INVAUT where IdInv = '" + set.getInt("IdInv") + "'")
                .getInt("IdAut");

        String author = connection.createStatement().executeQuery("SELECT * FROM AUTORI where IdAut = '" + idAuthor + "'")
                .getString("Aut");

        return new Book(
                s,
                author.toUpperCase(),
                prezzo,
                editoreSet.getString("Edi"),
                luogoSet.getString("Luo"),
                idInvElement,
                pub
        );
    }

    public ArrayList<Book> filterByTitle(String title) throws SQLException {
        ResultSet result = statement.executeQuery("SELECT * FROM LIBRI where Tit = '" + title.toUpperCase() + "'");
        ArrayList<Book> ret = new ArrayList<>();

        while (result.next()){
            ret.add(createBook(result));
        }
        return ret;
    }
}
